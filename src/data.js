var lists = {
	resources: [
		{id: "regolith", name: "Regolith"},
		{id: "helium3", name: "Helium-3"},
		{id: "metals", name: "Metals"},
		{id: "rare_metals", name: "Rare Metals"},
		{id: "minerals", name: "Minerals"},
		{id: "polymer", name: "Polymer"},
		{id: "electronics", name: "Electronics"},
		{id: "mechanical_parts", name: "Mechanical Parts"},
		{id: "ice", name: "Ice"},
		{id: "fuel", name: "Fuel"},
	],
	buildings: [
	// Extractors
		// Regolith Extractor
		{
			id: regolith_extractor,
			name: "Regolith Extractor",
			rates: [
				{resource: "regolith", quantity: 0.2},
				{resource: "helium3", quantity: 0.05}
			],
			energy: -5,
			price: {
				{resource: "regolith", quantity: 10},
				{resource: "mechanical_parts", quantity: 2}
			}
		},
		// Deep Miner
		{
			id: deep_miner,
			name: "Deep Miner",
			rates: [
				{resource: "minerals", quantity: 0.1},
				{resource: "ice", quantity: 0.02}
			],
			energy: -5,
			price: {
				{resource: "regolith", quantity: 10},
				{resource: "mechanical_parts", quantity: 1}
			}
		},
		// Ice Scraper
		{
			id: ice_scraper,
			name: "Ice Scraper",
			rates: [
				{resource: "ice", quantity: 0.1},
				{resource: "helium3", quantity: 0.01}
			],
			energy: -2,
			price: {
				{resource: "metals", quantity: 2},
				{resource: "mechanical_parts", quantity: 1}
			}
		},
	// Refiners
		// Smelter
		{
			id: smelter,
			name: "Smelter",
			energy: 15,
			price: {
				{resource: "regolith", quantity: 8},
				{resource: "mechanical_parts", quantity: 3}
			},
			rates: [
				{resource: "metals", quantity: 0.2},
				{resource: "rare_metals", quantity: 0.05},
			],
			raw: [
				{resource: "minerals", quantity: 0.2}
			]
		},
		// Electronics Assembly Line
		{
			id: electronics_assembly_line,
			name: "Electronics Assembly Line",
			energy: 15,
			price: {
				{resource: "regolith", quantity: 10},
				{resource: "mechanical_parts", quantity: 5}
				{resource: "electronics", quantity: 2}
			},
			rates: [
				{resource: "electronics", quantity: 1},
			],
			raw: [
				{resource: "polymer", quantity: 0.5}
				{resource: "rare_metals", quantity: 0.5}
			]
		},
		// Mechanical Parts Assembly Line
		{
			id: mechanical_parts_assembly_line,
			name: "Mechanical Parts Assembly Line",
			energy: 15,
			price: {
				{resource: "regolith", quantity: 10},
				{resource: "mechanical_parts", quantity: 5}
				{resource: "electronics", quantity: 2}
			},
			rates: [
				{resource: "mechanical_parts", quantity: 1},
			],
			raw: [
				{resource: "polymer", quantity: 0.2}
				{resource: "metals", quantity: 0.5}
			]
		},
		// Fuel Refinery
		{
			id: fuel_refinery,
			name: "Fuel Refinery",
			energy: 20,
			price: {
				{resource: "regolith", quantity: 10},
				{resource: "mechanical_parts", quantity: 1}
				{resource: "electronics", quantity: 3}
			},
			rates: [
				{resource: "fuel", quantity: 1},
			],
			raw: [
				{resource: "ice", quantity: 2}
			]
		},
		// Polymer Refinery
		{
			id: polymer_refinery,
			name: "Polymer Refinery",
			energy: 20,
			price: {
				{resource: "regolith", quantity: 10},
				{resource: "mechanical_parts", quantity: 1}
				{resource: "electronics", quantity: 3}
			},
			rates: [
				{resource: "polymer", quantity: 1},
			],
			raw: [
				{resource: "ice", quantity: 2}
				{resource: "helium3", quantity: 1}
			]
		},
	// Energy Producers
		// Wind Turbine
		{
			id: wind_turbine,
			name: "Wind Turbine",
			energy: 10,
			price: {
				{resource: "regolith", quantity: 5},
				{resource: "mechanical_parts", quantity: 1}
			}
		},
		// Solar Panel
		{
			id: solar_panel,
			name: "Solar Panel",
			energy: 2,
			price: {
				{resource: "regolith", quantity: 2},
				{resource: "polymer", quantity: 1}
			}
		},
		// RTG
		{
			id: rtg,
			name: "RTG",
			energy: 25,
			price: {
				{resource: "electronics", quantity: 2},
				{resource: "polymer", quantity: 15}
				{resource: "helium3", quantity: 30}
			}
		}
	]
}